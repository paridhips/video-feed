import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;
import com.google.android.exoplayer2.MediaItem;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.source.ProgressiveMediaSource;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory;
import com.google.android.exoplayer2.upstream.cache.CacheDataSourceFactory;
import com.google.android.exoplayer2.upstream.cache.SimpleCache;
import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MyPagerAdapter extends PagerAdapter {

    private Context context;
    private List<Video> videos;
    private SimpleExoPlayer player;
    private CacheDataSourceFactory cacheDataSourceFactory;
    private Map<Integer, Boolean> cachedMap; // Map to track cached videos
    private int maxCachedVideos = 3; // Maximum number of videos to cache ahead

    public MyPagerAdapter(Context context, List<Video> videos) {
        this.context = context;
        this.videos = videos;
        this.cachedMap = new HashMap<>();

        // Initialize cache
        File cacheDirectory = new File(context.getCacheDir(), "exoplayer_cache");
        SimpleCache simpleCache = new SimpleCache(cacheDirectory, new LeastRecentlyUsedCacheEvictor(maxCacheSize));
        cacheDataSourceFactory = new CacheDataSourceFactory(
            simpleCache,
            new DefaultHttpDataSourceFactory("userAgent"),
            CacheDataSource.FLAG_IGNORE_CACHE_ON_ERROR);
    }

    @Override
    public int getCount() {
        return videos.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View itemView = inflater.inflate(R.layout.item_viewpager, container, false);
        
        // Initialize ExoPlayer
        player = new SimpleExoPlayer.Builder(context).build();
        
        // Set up media source
        MediaItem mediaItem = MediaItem.fromUri(Uri.parse(videos.get(position).getUrl()));
        ProgressiveMediaSource mediaSource = new ProgressiveMediaSource.Factory(cacheDataSourceFactory)
                .createMediaSource(mediaItem);

        // Set media source to player
        player.setMediaSource(mediaSource);

        // Prepare the player
        player.prepare();

        // Add player to view
        container.addView(itemView);

        // Track cached videos
        cachedMap.put(position, true);

        // Cache next 3 videos if not already cached
        int nextVideoPosition = position + 1;
        while (nextVideoPosition < getCount() && nextVideoPosition <= position + maxCachedVideos) {
            if (!cachedMap.containsKey(nextVideoPosition)) {
                cacheVideo(nextVideoPosition);
            }
            nextVideoPosition++;
        }

        return itemView;
    }

    private void cacheVideo(int position) {
        MediaItem mediaItem = MediaItem.fromUri(Uri.parse(videos.get(position).getUrl()));
        ProgressiveMediaSource.Factory mediaSourceFactory = new ProgressiveMediaSource.Factory(cacheDataSourceFactory);
        ProgressiveMediaSource mediaSource = mediaSourceFactory.createMediaSource(mediaItem);
        player.setMediaSource(mediaSource);
        player.prepare();
        cachedMap.put(position, true);
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
        player.release(); // Release player when the item is destroyed
    }
}

