package dev.paridhi.videofeed.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;
import androidx.viewpager2.widget.ViewPager2;

import android.os.Bundle;
import android.util.Log;
import android.view.View;

import java.util.ArrayList;

import dev.paridhi.videofeed.R;
import dev.paridhi.videofeed.adapter.VideoFeedViewPagerAdapter;
import dev.paridhi.videofeed.api.RetrofitClient;
import dev.paridhi.videofeed.databinding.ActivityMainBinding;
import dev.paridhi.videofeed.model.DisplayData;
import dev.paridhi.videofeed.model.Msg;
import dev.paridhi.videofeed.model.RootNodeModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    ActivityMainBinding binding;
    RetrofitClient retrofitClient;

    VideoFeedViewPagerAdapter videoFeedViewPagerAdapter;
    ViewPager2 viewPager2;
    ArrayList<DisplayData> displayDataSet;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding=ActivityMainBinding.inflate(getLayoutInflater());
        View view=binding.getRoot();
        setContentView(view);

        Call<RootNodeModel> call=RetrofitClient.getInstance().getApi().getPosts();
        call.enqueue(new Callback<RootNodeModel>() {
            @Override
            public void onResponse(Call<RootNodeModel> call, Response<RootNodeModel> response) {

                ArrayList<Msg> messages=response.body().getMsg();
                Log.e("VideoApi","Size of message "+messages.size());
                displayDataSet=new ArrayList<>();
                messages.forEach((message)->{
                    DisplayData displayData=new DisplayData(message.getDescription(),message.getVideo());
                    displayDataSet.add(displayData);
                });

                viewPager2=binding.videoFeedVP;

                videoFeedViewPagerAdapter=new VideoFeedViewPagerAdapter(getApplicationContext(),displayDataSet);

                viewPager2.setAdapter(videoFeedViewPagerAdapter);

                Log.e("VideoApi","Size of display "+displayDataSet.size());

            }

            @Override
            public void onFailure(Call<RootNodeModel> call, Throwable t) {
                //Log.e("VideoApi", t.getMessage());

            }
        });

    }

    @Override
    protected void onPause() {
        super.onPause();

    }
}