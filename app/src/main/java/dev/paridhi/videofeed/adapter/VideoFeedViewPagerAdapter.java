package dev.paridhi.videofeed.adapter;

import android.content.Context;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.MediaItem;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.source.ProgressiveMediaSource;
import com.google.android.exoplayer2.ui.DefaultTimeBar;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.ui.StyledPlayerView;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.cache.Cache;
import com.google.android.exoplayer2.upstream.cache.LeastRecentlyUsedCacheEvictor;
import com.google.android.exoplayer2.upstream.cache.SimpleCache;
import com.google.android.exoplayer2.video.VideoDecoderGLSurfaceView;


import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import dev.paridhi.videofeed.Helper.ExoPlayerCacheHelper;
import dev.paridhi.videofeed.R;
import dev.paridhi.videofeed.model.DisplayData;

public class VideoFeedViewPagerAdapter extends RecyclerView.Adapter<VideoFeedViewPagerAdapter.ViewHolder> {

    private Context context;
    private List<DisplayData> displayDataList;
    private SimpleExoPlayer player;
    private SimpleExoPlayer prevPlayer;
    private DataSource.Factory dataSourceFactory;
    private Map<Integer, Boolean> cachedMap;
    private Cache cache;
    private int maxCachedVideos = 3;

    public VideoFeedViewPagerAdapter(Context context, List<DisplayData> displayDataList) {
        this.context = context;
        this.displayDataList = displayDataList;
        this.dataSourceFactory = ExoPlayerCacheHelper.buildDataSourceFactory(context);
        this.cachedMap = new HashMap<>();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_feed, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull VideoFeedViewPagerAdapter.ViewHolder holder, int position) {
        holder.bind(position);


    }



    @Override
    public int getItemCount() {
        return displayDataList.size();
    }

   public class ViewHolder extends RecyclerView.ViewHolder {
       TextView descTextView;
       StyledPlayerView styledPlayerView;


       public ViewHolder(@NonNull View itemView) {
           super(itemView);
           descTextView = itemView.findViewById(R.id.video_feed_item_video_description);
           styledPlayerView=itemView.findViewById(R.id.video_feed_item_video_player);
       }

       public void bind(int position) {



           player=new SimpleExoPlayer.Builder(context).build();

           // Set up media source
           MediaItem mediaItem = MediaItem.fromUri(Uri.parse(displayDataList.get(position).getVideoURL()));
           ProgressiveMediaSource mediaSource = new ProgressiveMediaSource.Factory(dataSourceFactory)
                   .createMediaSource(mediaItem);

           Log.e("MediaItem",displayDataList.get(position).getVideoURL());
           // Set media source to player

           player.setMediaSource(mediaSource);
           styledPlayerView.setPlayer(player);


           descTextView.setText(displayDataList.get(position).getDescription());

           cacheNextVideos(position);
       }
       public void startPlayer() {
           player.prepare();
           player.setPlayWhenReady(true);
           player.getPlaybackState();
       }
       public void releasePlayer() {
           player.stop();
           player.release();
       }


   }
       private void cacheNextVideos(int position) {
           int nextVideoPosition = position + 1;
           while (nextVideoPosition < getItemCount() && nextVideoPosition <= position + maxCachedVideos) {
               if (!cachedMap.containsKey(nextVideoPosition)) {
                   cacheVideo(nextVideoPosition);
               }
               nextVideoPosition++;
           }
       }


    private void cacheVideo(int position) {
        MediaItem mediaItem = MediaItem.fromUri(Uri.parse(displayDataList.get(position).getVideoURL()));
        ProgressiveMediaSource.Factory mediaSourceFactory = new ProgressiveMediaSource.Factory(dataSourceFactory);
        ProgressiveMediaSource mediaSource = mediaSourceFactory.createMediaSource(mediaItem);
        player.setMediaSource(mediaSource);
        cachedMap.put(position, true);
    }

    @Override
    public void onViewAttachedToWindow(@NonNull ViewHolder holder) {
        super.onViewAttachedToWindow(holder);
        holder.startPlayer();


    }

    @Override
    public void onViewDetachedFromWindow(@NonNull ViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        holder.releasePlayer();
    }
}
