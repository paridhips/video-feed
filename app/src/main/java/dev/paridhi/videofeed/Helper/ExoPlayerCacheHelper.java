package dev.paridhi.videofeed.Helper;

import android.content.Context;

import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.upstream.cache.Cache;
import com.google.android.exoplayer2.upstream.cache.CacheDataSource;
import com.google.android.exoplayer2.upstream.cache.LeastRecentlyUsedCacheEvictor;
import com.google.android.exoplayer2.upstream.cache.SimpleCache;

import java.io.File;

public class ExoPlayerCacheHelper {
    private static final String CACHE_DIR = "exoplayer_cache";
    private static final long MAX_CACHE_SIZE = 100 * 1024 * 1024; // 100 MB
    private static Cache cache;

    public static DataSource.Factory buildDataSourceFactory(Context context) {

        if (cache == null) {
            cache = new SimpleCache(new File(context.getCacheDir(), CACHE_DIR), new LeastRecentlyUsedCacheEvictor(MAX_CACHE_SIZE));
        }


        // Create a data source factory
        DefaultDataSourceFactory defaultDataSourceFactory = new DefaultDataSourceFactory(context);

        // Create a CacheDataSource.Factory
        CacheDataSource.Factory cacheDataSourceFactory = new CacheDataSource.Factory()
                .setCache(cache)
                .setUpstreamDataSourceFactory(defaultDataSourceFactory);

        return cacheDataSourceFactory;
    }
}
