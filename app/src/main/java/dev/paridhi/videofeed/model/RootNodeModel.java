package dev.paridhi.videofeed.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class RootNodeModel {
        @SerializedName("s")
        String s;
        @SerializedName("code")
        String code;
        @SerializedName("msg")
        ArrayList<Msg> msg;


        public RootNodeModel(String s, String code, ArrayList<Msg> msg) {
                this.s = s;
                this.code = code;
                this.msg = msg;
        }

        public String getS() {
                return s;
        }

        public void setS(String s) {
                this.s = s;
        }

        public String getCode() {
                return code;
        }

        public void setCode(String code) {
                this.code = code;
        }

        public ArrayList<Msg> getMsg() {
                return msg;
        }

        public void setMsg(ArrayList<Msg> msg) {
                this.msg = msg;
        }
}
