package dev.paridhi.videofeed.model;

public class DisplayData {
    String description;
    String videoURL;

    public DisplayData() {
    }

    public DisplayData(String description, String videoURL) {
        this.description = description;
        this.videoURL = videoURL;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getVideoURL() {
        return videoURL;
    }

    public void setVideoURL(String videoURL) {
        this.videoURL = videoURL;
    }
}
