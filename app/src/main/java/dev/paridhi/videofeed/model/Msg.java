package dev.paridhi.videofeed.model;

import com.google.gson.annotations.SerializedName;

import java.util.Date;



public class Msg {

    @SerializedName("tp")
   int tp;

    @SerializedName("uid")
    String uid;

    @SerializedName("liked")
    int liked;
    @SerializedName("score")
    int score;

    @SerializedName("status")
    String status;
    @SerializedName("is_block")
    int is_block;

    @SerializedName("description")
    String description;

    @SerializedName("country")
    String country;
    @SerializedName("city")
    String city;
    @SerializedName("_id")
    String _id;

    @SerializedName("id")
    int id;

    @SerializedName("fb_id")
    String fb_id;

    @SerializedName("user_info")
    UserInfo user_info;
    @SerializedName("count")
    Count count;

    @SerializedName("video")
    String video;

    @SerializedName("thum")
    String thum;

    @SerializedName("gif")
    String gif;

    @SerializedName("sound")
    Sound sound;

    @SerializedName("created")
    Date created;
    @SerializedName("__v")
    int __v;

    public Msg(int tp, String uid, int liked, int score, String status, int is_block, String description, String country, String city, String _id, int id, String fb_id, UserInfo user_info, Count count, String video, String thum, String gif, Sound sound, Date created, int __v) {
        this.tp = tp;
        this.uid = uid;
        this.liked = liked;
        this.score = score;
        this.status = status;
        this.is_block = is_block;
        this.description = description;
        this.country = country;
        this.city = city;
        this._id = _id;
        this.id = id;
        this.fb_id = fb_id;
        this.user_info = user_info;
        this.count = count;
        this.video = video;
        this.thum = thum;
        this.gif = gif;
        this.sound = sound;
        this.created = created;
        this.__v = __v;
    }

    public int getTp() {
        return tp;
    }

    public void setTp(int tp) {
        this.tp = tp;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public int getLiked() {
        return liked;
    }

    public void setLiked(int liked) {
        this.liked = liked;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getIs_block() {
        return is_block;
    }

    public void setIs_block(int is_block) {
        this.is_block = is_block;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFb_id() {
        return fb_id;
    }

    public void setFb_id(String fb_id) {
        this.fb_id = fb_id;
    }

    public UserInfo getUser_info() {
        return user_info;
    }

    public void setUser_info(UserInfo user_info) {
        this.user_info = user_info;
    }

    public Count getCount() {
        return count;
    }

    public void setCount(Count count) {
        this.count = count;
    }

    public String getVideo() {
        return video;
    }

    public void setVideo(String video) {
        this.video = video;
    }

    public String getThum() {
        return thum;
    }

    public void setThum(String thum) {
        this.thum = thum;
    }

    public String getGif() {
        return gif;
    }

    public void setGif(String gif) {
        this.gif = gif;
    }

    public Sound getSound() {
        return sound;
    }

    public void setSound(Sound sound) {
        this.sound = sound;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public int get__v() {
        return __v;
    }

    public void set__v(int __v) {
        this.__v = __v;
    }
}
