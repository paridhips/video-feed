package dev.paridhi.videofeed.model;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class Sound {

    @SerializedName("id")
    int id;
    @SerializedName("sound_name")
    String sound_name;
    @SerializedName("description")
    String description;
    @SerializedName("thum")
    String thum;

    @SerializedName("section")
    String section;
    @SerializedName("_id")
    String _id;

    @SerializedName("created")
    Date created;
    @SerializedName("audio_path")
    AudioPath audio_path;

    public Sound(int id, String sound_name, String description, String thum, String section, String _id, Date created, AudioPath audio_path) {
        this.id = id;
        this.sound_name = sound_name;
        this.description = description;
        this.thum = thum;
        this.section = section;
        this._id = _id;
        this.created = created;
        this.audio_path = audio_path;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSound_name() {
        return sound_name;
    }

    public void setSound_name(String sound_name) {
        this.sound_name = sound_name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getThum() {
        return thum;
    }

    public void setThum(String thum) {
        this.thum = thum;
    }

    public String getSection() {
        return section;
    }

    public void setSection(String section) {
        this.section = section;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public AudioPath getAudio_path() {
        return audio_path;
    }

    public void setAudio_path(AudioPath audio_path) {
        this.audio_path = audio_path;
    }
}
