package dev.paridhi.videofeed.api;

import dev.paridhi.videofeed.model.RootNodeModel;
import retrofit2.Call;
import retrofit2.http.GET;

public interface ApiInterface {

    @GET("app_api/index.php?p=showAllVideos")
    Call<RootNodeModel> getPosts();
}
